# Android_enable_nativebridge

#### 介绍
本来是自用的，开源是为了方便大家下载，内包含三个文件，把文件上传至 android x86 7.1 的system/etc目录中，然后从终端依次执行以下命令
```
su
enable_nativebridge
enable_nativebridge 64
```
然后从设置里开启arm兼容就可以了，如果不行的话就反复执行、开关、重启几次。
